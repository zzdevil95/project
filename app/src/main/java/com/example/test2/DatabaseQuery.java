package com.example.test2;

import android.content.Context;
import android.database.Cursor;
import com.example.test2.EventObjects;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
public class DatabaseQuery extends DatabaseObject{
    private static final String TAG = database.class.getSimpleName();
    private final String USER_TABLE = "timetable";
    public DatabaseQuery(Context context) {
        super(context);
    }
    private Date convertStringToDate(String dateInString){
        DateFormat format = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
        Date date = null;
        try {
            date = format.parse(dateInString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public List<EventObjects> getAllFutureEvents(){
        Date dateToday = new Date();
        List<EventObjects> events = new ArrayList<>();
        String query = "select * from timetable";
        Cursor cursor = this.getDbConnection().rawQuery(query, null);
        if(cursor.moveToFirst()){
            do{
                int id = cursor.getInt(0);
                String password = cursor.getString(cursor.getColumnIndexOrThrow("password"));
                String SubjectCode = cursor.getString(cursor.getColumnIndexOrThrow("SubjectCode"));
                String SubjectName = cursor.getString(cursor.getColumnIndexOrThrow("SubjectName"));
                String examTime = cursor.getString(cursor.getColumnIndexOrThrow("examTime"));
                String startDate = cursor.getString(cursor.getColumnIndexOrThrow("examDate"));
                //convert start date to date object
                Date reminderDate = convertStringToDate(startDate);

                    events.add(new EventObjects(id, password, SubjectCode, SubjectName, examTime, reminderDate));

            }while (cursor.moveToNext());
        }
        cursor.close();
        return events;
    }
    public boolean checkUserExist(String userID, String password) {
        String[] columns = {"id"};

        String selection = "id=? and password=?";
        String[] selectionArgs = {userID, password};

        Cursor cursor = this.getDbConnection().query(USER_TABLE, columns, selection, selectionArgs, null, null, null);
        int count = cursor.getCount();

        cursor.close();


        if (count > 0) {
            return true;
        } else {
            return false;
        }
    }
    public Cursor getSubjectDetail(){

        Cursor c = this.getDbConnection().rawQuery("SELECT SubjectCode, SubjectName, examTime FROM timetable WHERE id = 19197844", null);
        return c;
    }
    public Cursor getSubjectDetail2(){

        Cursor c = this.getDbConnection().rawQuery("SELECT SubjectCode, SubjectName, examTime FROM timetable WHERE id = 19197123", null);
        return c;
    }
    }
