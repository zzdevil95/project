package com.example.test2;


import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;


/**
 * A simple {@link Fragment} subclass.
 */
public class webfragment extends Fragment{


    public webfragment() {
        // Required empty public constructor
    }
    private WebView wb;


    @SuppressLint("JavascriptInterface")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_webfragment, container, false);
        wb = (WebView)v.findViewById(R.id.webpage);
        wb.loadUrl("https://login.navigate.navitas.com/ltusc/index/forgot-password");
        WebSettings settings = wb.getSettings();
        settings.setJavaScriptEnabled(true);
        wb.setWebViewClient(new WebViewClient());

        return v;
    }


}
