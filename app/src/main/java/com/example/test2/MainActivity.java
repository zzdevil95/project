package com.example.test2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button ForgotPassword;
    Button btnLogin;
    EditText edtUserID;
    EditText edtPassword;
    DatabaseQuery db;
    String st;

    private static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        edtUserID = (EditText) findViewById(R.id.edtUserID);
        edtPassword = (EditText) findViewById(R.id.edtPassword);
        ForgotPassword = (Button)findViewById(R.id.ForgotPassword);


        db = new DatabaseQuery(MainActivity.this);

        btnLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                boolean isExist = db.checkUserExist(edtUserID.getText().toString(), edtPassword.getText().toString());

                if (isExist) {
                    Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                    st = edtUserID.getText().toString();
                    intent.putExtra("id", st);
                    startActivity(intent);
                } else {
                    edtPassword.setText(null);
                    Toast.makeText(MainActivity.this, "Login Failed.", Toast.LENGTH_SHORT).show();
                }
            }
        });
        ForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                webfragment web = new webfragment();
                fragmentTransaction.add(R.id.myFrame, web, "Website");
                fragmentTransaction.commit();


            }
        });


        }

    }
