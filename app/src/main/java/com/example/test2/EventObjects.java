package com.example.test2;

import java.util.Date;
public class EventObjects {
    private int id;
    public String password, subjectCode, subjectName, examTime;
    private Date examDate;
    public EventObjects(String password, String subjectCode, String subjectName, String examTime, Date examDate) {
        this.password = password;
        this.subjectCode = subjectCode;
        this.subjectName = subjectName;
        this.examTime = examTime;
        this.examDate = examDate;
    }
    public EventObjects(int id, String password, String subjectCode, String subjectName, String examTime, Date examDate) {
        this.password = password;
        this.subjectCode = subjectCode;
        this.subjectName = subjectName;
        this.examTime = examTime;
        this.examDate = examDate;
        this.id = id;
    }
    public int getId() {
        return id;
    }
    public String getPassword() {
        return password;
    }
    public Date getDate() {
        return examDate;
    }
    public String getSubjectCode(){return subjectCode;}
    public String getSubjectName(){return subjectName;}
    public String getExamTime(){return examTime;}
}